package adventofcode2017

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
)

func linesFromFilename(filename string) ([]string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return []string{}, err
	}
	return linesFromReader(f)
}

func linesFromReader(r io.Reader) ([]string, error) {
	var lines []string
	sc := bufio.NewScanner(r)
	for sc.Scan() {
		line := sc.Text()
		lines = append(lines, line)
	}
	return lines, nil
}

func exampleFilename(day int) string {
	return fmt.Sprintf("testdata/day%d_example.txt", day)
}

func filename(day int) string {
	return fmt.Sprintf("testdata/day%d.txt", day)
}

// linesAsNumber converts strings into integer.
func linesAsNumbers(lines []string) ([]int, error) {
	var is []int
	for i := range lines {
		n, err := strconv.Atoi(lines[i])
		if err != nil {
			return is, fmt.Errorf("error in line %d: cannot convert %q to number",
				i, lines[i])
		}
		is = append(is, n)
	}
	return is, nil
}
