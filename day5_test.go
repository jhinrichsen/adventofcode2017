package adventofcode2017

import (
	"bufio"
	"os"
	"strconv"
	"testing"
)

func TestDay05Part1Example(t *testing.T) {
	maze := []int{0, 3, 0, 1, -3}
	want := 5
	got := Day05Part1(maze)
	if want != got {
		t.Fatalf("want %d but got %d\n", want, got)
	}
}

func TestDay05Part2Example(t *testing.T) {
	maze := []int{0, 3, 0, 1, -3}
	want := 10
	got := Day05Part2(maze)
	if want != got {
		t.Fatalf("want %d but got %d\n", want, got)
	}
}

func TestDay05Part1(t *testing.T) {
	want := 355965
	got := Day05Part1(maze())
	if want != got {
		t.Fatalf("want %d but got %d\n", want, got)
	}
}

func TestDay05Part2(t *testing.T) {
	want := 26948068
	got := Day05Part2(maze())
	if want != got {
		t.Fatalf("want %d but got %d\n", want, got)
	}
}

func maze() []int {
	f, err := os.Open("testdata/day5.txt")
	if err != nil {
		panic(err)
	}
	var maze []int
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		s := sc.Text()
		// overread empty lines
		if len(s) == 0 {
			continue
		}
		i, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}
		maze = append(maze, i)
	}
	return maze
}
